package com.daffodil.system.service;

import java.util.List;

import com.daffodil.core.entity.Query;
import com.daffodil.system.entity.SysDictData;

/**
 * 字典 业务层
 * 
 * @author yweijian
 * @date 2019年8月16日
 * @version 1.0
 */
public interface ISysDictDataService {
	
	/**
	 * 根据条件分页查询字典数据
	 * @param query
	 * @return
	 */
	public List<SysDictData> selectDictDataList(Query<SysDictData> query);
	
	/**
	 * 根据字典类型查询字典数据
	 * @param dictType
	 * @return
	 */
	public List<SysDictData> selectDictDataByType(String dictType);

	/**
	 * 根据字典类型和字典键值查询字典数据信息
	 * @param dictType
	 * @param dictValue
	 * @return
	 */
	public String selectDictLabel(String dictType, String dictValue);

	/**
	 * 根据字典数据ID查询信息
	 * @param dictId
	 * @return
	 */
	public SysDictData selectDictDataById(String dictId);

	/**
	 * 批量删除字典数据
	 * @param ids
	 */
	public void deleteDictDataByIds(String[] ids);

	/**
	 * 新增保存字典数据信息
	 * @param dictData
	 */
	public void insertDictData(SysDictData dictData);

	/**
	 * 修改保存字典数据信息
	 * @param dictData
	 */
	public void updateDictData(SysDictData dictData);
}
