package com.daffodil.system.service;

import java.util.List;

import com.daffodil.core.entity.Query;
import com.daffodil.core.entity.Ztree;
import com.daffodil.system.entity.SysDictType;

/**
 * 字典类型 业务层
 * @author yweijian
 * @date 2019年12月18日
 * @version 1.0
 */
public interface ISysDictTypeService {
	
	/**
	 * 根据条件分页查询字典类型
	 * @param query
	 * @return
	 */
	public List<SysDictType> selectDictTypeList(Query<SysDictType> query);
	
	/**
	 * 根据字典类型ID查询信息
	 * @param dictTypeId
	 * @return
	 */
	public SysDictType selectDictTypeById(String dictTypeId);

	/**
	 * 根据字典类型查询信息
	 * @param dictType
	 * @return
	 */
	public SysDictType selectDictTypeByType(String dictType);

	/**
	 * 批量删除字典类型
	 * @param ids
	 */
	public void deleteDictTypeByIds(String[] ids);

	/**
	 * 新增保存字典类型信息
	 * @param dictType
	 */
	public void insertDictType(SysDictType dictType);

	/**
	 * 修改保存字典类型信息
	 * @param dictType
	 */
	public void updateDictType(SysDictType dictType);

	/**
	 * 校验字典类型称是否唯一
	 * @param dictType
	 * @return
	 */
	public boolean checkDictTypeUnique(SysDictType dictType);

	/**
	 * 查询字典类型树
	 * @param dictType
	 * @return
	 */
	public List<Ztree> selectDictTree(SysDictType dictType);
}
